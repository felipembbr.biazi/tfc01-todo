package com.example.todo.ui.components.task

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.todo.R
import com.example.todo.model.Task
import com.example.todo.ui.theme.Betwen
import com.example.todo.ui.theme.White

@Composable
fun TaskItemView(task: Task)
{
    Card(modifier =
    Modifier
        .fillMaxWidth()
        .height(IntrinsicSize.Min)
        .padding(vertical = 2.dp, horizontal = 2.dp),
        backgroundColor = MaterialTheme.colors.primary
        ) {
            Row() {
                Box(

                    modifier =
                Modifier
                    .fillMaxHeight()
                    .defaultMinSize(minHeight = 60.dp)
                    .width(25.dp)
                    .clip(RoundedCornerShape(4.dp))
                    .background(
                        if(task.isUrgent) Color.Red
                        else Color.Green
                    )
                )
                Text(
                    text = task.txt_task,
                    fontSize = 25.sp,
                    maxLines = Int.MAX_VALUE,
                    overflow = TextOverflow.Ellipsis,
                    modifier =
                    Modifier
                        .padding(start = 8.dp, top = 5.dp, bottom = 5.dp)
                        .align(Alignment.CenterVertically)
                        .fillMaxWidth().weight(1f)
                )
                AddCheckBox(
                    task = task,
                    modifier = Modifier.align(Alignment.CenterVertically)
                )
            }
    }
}

@Composable
fun AddCheckBox(task: Task, modifier: Modifier = Modifier)
{
    var isChecked by remember{ mutableStateOf(task.isDone)}
    Image(
        painter =
        painterResource(id =
        if(isChecked)
            R.drawable.ic_inner_checkbox
        else
            R.drawable.ic_outline_checkbox
        ),
        contentDescription = stringResource(id = R.string.checked),
        modifier =
            modifier.size(32.dp).clickable {
                isChecked = !isChecked
                task.isDone = isChecked
            },
        colorFilter =
            ColorFilter.tint(
                color = White
            )
    )
    Spacer(modifier = Modifier.width(5.dp))
}

@Composable
@Preview
fun PreviewTest()
{
    val task = Task(
        "testando a telaaaaaaaaaaaa ajnkjdsdaaaa",
        true,
        true
    )
    TaskItemView(task = task)
}