package com.example.todo.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)


val Urgente = Color(0xFFF10606)
val Nurgente = Color(0xFF04F80E)
val White = Color(0xFFFFFFFF)
val Betwen = Color(0xFF1CF5C2)