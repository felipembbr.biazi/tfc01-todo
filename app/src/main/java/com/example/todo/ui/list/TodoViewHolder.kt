package com.example.todo.ui.list

import android.view.View
import android.widget.CheckBox
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.model.Task

class TodoViewHolder (
    itemView: View,
    private val adapter: TodoAdapter
):RecyclerView.ViewHolder(itemView) {
    private val Check: CheckBox
    private val TextTask: TextView
    private val flUrgente: FrameLayout
    private lateinit var currentTask: Task
    init{
        this.Check=itemView.findViewById((R.id.checkBox))
        this.TextTask=itemView.findViewById(R.id.textView)
        this.flUrgente = itemView.findViewById(R.id.cr_item)
        Check.setOnCheckedChangeListener{_, done ->
            currentTask.isDone = done

        }
    }

    fun bind(task: Task){
        this.currentTask = task
        this.TextTask.text = this.currentTask.txt_task
        this.Check.isChecked = this.currentTask.isDone
        val color = if (this.currentTask.isUrgent){
            ContextCompat.getColor(itemView.context,R.color.red)
        }else{
            ContextCompat.getColor(itemView.context,R.color.green)
        }
        flUrgente.background.setTint(color)

    }


}