package com.example.todo.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.model.Task

class TodoAdapter (
    private val tarefas: ArrayList<Task>
): RecyclerView.Adapter<TodoViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(
            R.layout.item_view,
            parent,
            false
        )
        return TodoViewHolder(itemView, this)
    }

    fun addTask(todo: Task)
    {
        tarefas.add(todo)
        notifyItemInserted(tarefas.size-1)
    }
    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        holder.bind(this.tarefas[position])
    }

    override fun getItemCount(): Int {
        return this.tarefas.size
    }
}