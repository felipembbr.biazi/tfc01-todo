package com.example.todo


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Adapter
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.model.Task
import com.example.todo.ui.list.TodoAdapter


class MainActivity : AppCompatActivity() {
    private lateinit var adaptar: TodoAdapter
    private lateinit var text_task: TextView
    private lateinit var Urgent: SwitchCompat
    private lateinit var tarefas: ArrayList<Task>
    private lateinit var resicler: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.Urgent=findViewById(R.id.swt_urg)
        this.text_task=findViewById(R.id.etxtMessage)
        this.resicler=findViewById(R.id.rv_mostrar)
        this.tarefas= ArrayList()
        this.adaptar = TodoAdapter(this.tarefas)
        this.resicler.layoutManager = LinearLayoutManager(this)
        this.resicler.adapter = this.adaptar


    }

    fun OnClickAdd(V: View)
    {
        val texto:String= this.text_task.text.toString()
        if (Urgent.isChecked)
        {
            if(texto.isNotEmpty())
            {
                val tarefa = Task(texto, false,true)
                this.adaptar.addTask(tarefa)
                this.text_task.setText("")
            }
        }else{
            if(texto.isNotEmpty())
            {
                val tarefa = Task(texto,false,false)
                this.adaptar.addTask(tarefa)
                this.text_task.setText("")
            }
        }
    }

}